
public class BinaryTreeProgramme {

	public static void main(String[] args) {
		BinaryTreeNode root = new BinaryTreeNode(5);
		root.leftNode = new BinaryTreeNode(1);
		root.rightNode = new BinaryTreeNode(3);
		System.out.println();
	}

}
