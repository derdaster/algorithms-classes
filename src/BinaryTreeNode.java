
public class BinaryTreeNode {
	int value;
	BinaryTreeNode leftNode;
	BinaryTreeNode rightNode;

	public BinaryTreeNode(int value) {
		this.value = value;
	}

}
